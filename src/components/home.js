import React, { useEffect } from "react";
import { connect, styled } from "frontity";
import FeaturedMedia from "./featured-media";
import Text from "./home/text";
import Newses from "./home/newses";
import Slider from "./home/slider";
import SliderCircles from "./home/slider-circles";
import HeroHome from "./home/hero";
import Header from "./header/index";
import Footer from "./footer/index";

const Home = ({ state, actions, libraries }) => {
  // Get information about the current URL.
  const data = state.source.get(state.router.link);
  // Get the data of the post.
  const post = state.source[data.type][data.id];
  const excerpt = post.excerpt;
  const sections = post.acf.sections;
  let classname = "";

  // Get the html2react component.
  const Html2React = libraries.html2react.Component;
  const dataNewses = state.source.get("/latest-news");

  // Load the post, but only if the data is ready.
  return data.isReady ? (
    <>
      <Header />
      <HeroHome />
      {sections && (
        <>
          {sections.map((section, i) => {
            if (section.acf_fc_layout === "section_text")
              return <Text section={section} key={0} />

            if (section.acf_fc_layout === "section_slider")
              return <Slider section={section} key={1} />

            if (section.acf_fc_layout === "section_slider_circles")
              return <SliderCircles section={section} key={2} />

            if (section.acf_fc_layout === "latest_news")
              return <Newses section={section} key={3} />
          })}
        </>
      )}
    <Footer />
    </>
  ) : null;

};



export default connect(Home);
