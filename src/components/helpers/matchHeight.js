import ResponsiveAutoHeight from "responsive-auto-height";

export const matchHeight = () => {
  const mhAttributesContainers = document.querySelectorAll("[data-h]");

  if (mhAttributesContainers) {
      const attributesArr = [];

      [...mhAttributesContainers].map(block => {
          attributesArr.push(block.getAttribute("data-h"));
      });
      const attributes = attributesArr.filter((x, i, a) => a.indexOf(x) == i);

      attributes.forEach(value => {
          new ResponsiveAutoHeight("[data-h="+value+"]");
      });
  }
}