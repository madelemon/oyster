import React, { useEffect } from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link";
import FeaturedMedia from "./featured-media";
import Text from "./post/text";
import Gallery from "./post/gallery";
import Thumbnail from "./post/thumbnail";
import Hero from "./page/hero";
import Header from "./header/index";
import Footer from "./footer/index";

const Post = ({ state, actions, libraries }) => {
  // Get information about the current URL.
  const data = state.source.get(state.router.link);
  // Get the data of the post.
  const post = state.source[data.type][data.id];
  const title = post.title.rendered;
  const excerpt = post.excerpt;
  const sections = post.acf.sections;
  let classname = "";

  // Get the html2react component.
  const Html2React = libraries.html2react.Component;

  // Load the post, but only if the data is ready.
  return data.isReady ? (
    <>
      <Header />
      <Hero />
      <section className="py-5">
        <div className="container-fluid">
          <div className="row">
            <h1 className="col-md-7 offset-md-4" data-aos="fade">{title}</h1>
            <div className="col-md-7 offset-md-4 mb-5 pe-60">
              {post.excerpt && (
                <Excerpt
                  className="lh-1-3"
                  data-aos="fade"
                  data-aos-delay="200"
                  dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }}
                />
              )}
            </div>

            {sections && (
              <>
                {sections.map((section, i) => {
                  if (section.acf_fc_layout === "text")
                    return <Text section={section} key={i} />;

                  if (section.acf_fc_layout === "gallery")
                    return <Gallery section={section} key={i} />;
                  
                if (section.acf_fc_layout === "thumbnail")
                    return <Thumbnail media={post.featured_media} key={i} />;
                })}
              </>
            )}
          </div>
        </div>
      </section>
      <Footer />
    </>
  ) : null;
};



export default connect(Post);

const Excerpt = styled.div`
  font-size: 1.5rem;

  @media screen and (max-width: 992px) {
    font-size: 1.125rem;
  }

  *:last-child {
    margin-bottom: 0;
  }
`

