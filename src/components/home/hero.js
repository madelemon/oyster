import React, { useRef, useLayoutEffect, useState, useEffect } from "react";
import { connect, styled } from "frontity"
import FeaturedMedia from "../featured-media";
import logoW from "../../assets/img/static/logo.svg";
import {isMobile} from "../helpers/isMobile";

const isSafari = () => {
  const ua = navigator.userAgent.toLowerCase();
  return ua.indexOf("safari") > -1 && ua.indexOf("chrome") < 0;
};

const HeroHome = ({ state }) => {
  const data = state.source.get(state.router.link)
  const page = state.source[data.type][data.id]
  const videoUrl = page.acf.video + '?v=2.0';
  const title = page.title.rendered;
  const text1 = page.acf.hero_text1;
  const text2 = page.acf.hero_text2;  
  const mediaId = state.source.[data.type][data.id].featured_media;
  const media = state.source.attachment[mediaId].source_url;

  let media_url = "";
  let backgroundImageStyle = "";

  //* video *//
  const videoParentRef = useRef();
  //* video *//


  const heroHome = useRef(null);

  if(media !== undefined) {
    backgroundImageStyle = {
        backgroundImage: "url(" + media + ")"
    };
  }

  useEffect(() => {
    //* video *//
    if (isSafari() && videoParentRef.current) {
      const player = videoParentRef.current.children[0];

      if (player) {
        player.controls = false;
        player.playsinline = true;
        player.muted = true;
        player.setAttribute("muted", "");
        player.autoplay = true;

        setTimeout(() => {
          const promise = player.play();
          if (promise.then) {
            promise
              .then(() => {})
              .catch(() => {
                //videoParentRef.current.style.display = "none";
              });
          }
        }, 0);
      }
    }
    //* video *//
   }, [])

  return (
    <div>
      <Hero className="hero-home" style={backgroundImageStyle} ref={heroHome}>
        <BgLayer />
        {videoUrl && (
          <Video className="pin-video"
            ref={videoParentRef}
            dangerouslySetInnerHTML={{
              __html: `
              <video
                loop
                muted
                autoplay
                playsinline
                src="${videoUrl}"
              >
              </video>`
            }} />
        )}
        <ScrollDown href="#section0" className="scroll-down text-uppercase text-white" title="Scroll down">scroll down</ScrollDown>
        <div className="container-fluid text-white position-relative text-uppercase">
          <div className="row align-items-start h-100 flex-column justify-content-between">
            <div className="col-md-3 offset-md-2 hero-layer hero-layer1" data-aos="fade">
              <div className="paragraph" dangerouslySetInnerHTML={{ __html: text1 }}></div>
            </div>
            <div className="col-8 col-md-9 offset-md-2 hero-layer hero-layer2" data-aos="fade" data-aos-delay="200">
              <img src={logoW} alt="Oyster" className="hero-logo" />
            </div>
            <div className="col-md-5 offset-md-6 hero-layer hero-layer3" data-aos="fade" data-aos-delay="400">
              <div className="paragraph" dangerouslySetInnerHTML={{ __html: text2 }}></div>
            </div>
          </div>
        </div>
      </Hero>
    </div>
  )
}

export default connect(HeroHome)


export const BgLayer = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: -10rem;
  background: var(--bs-dark);
  opacity: .2;
  z-index: 2;
`

const ScrollDown = styled.a`
  position: absolute;
  bottom: 3.125rem;
  letter-spacing: 1px;
  writing-mode: vertical-lr;
  left: 8.44%;
  cursor: pointer;
  z-index: 9;

  &:hover:before {
    transform: scaleY(0);
  }

  &:before {
    content: '';
    height: 12.5rem;
    width: 1px;
    background: #c9c9c9;
    opacity: .3;
    position: absolute;
    bottom: calc(100% + 1.25rem);
    transform-origin: center bottom;
    left: 50%;
    transition: all .3s ease-in-out;
  }
`

const Video = styled.div`
  margin-top: -7rem;

  video {
    pointer-events: none;
    position: fixed;
    width: 100%;
    height: 100vh;
    object-fit: cover;
    object-position: 50% 50%;

    @media screen and (max-width: 575px) {
      height: 120vh;
    }
  }
`

const Hero = styled.section`
  background-color: var(--bs-dark);
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  min-height: 100vh;
  display: flex;
  padding-top: 7rem;
  padding-bottom: 4rem;
  position: relative;

  @media screen and (min-width: 769px) {
    background-attachment: fixed;
  }

  @media screen and (max-width: 768px) {
    padding-top: 8rem;
    padding-bottom: 2rem;
  }

  .container-fluid {
    position: relative;
    z-index: 4;
    max-width: 100vw !important;
  }

  h1 {
    margin-bottom: 0;
  }

  .hero-layer {
    padding-bottom: 1rem;//4.25rem;

    .paragraph {
      line-height: 1.23;
      font-size: 1rem;
      letter-spacing: 1px;
      font-family: 'CeraPro-Medium';
    }

    img {
      max-height: 12.5rem;
    }
  }

`