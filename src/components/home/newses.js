import React from "react"
import { connect, styled } from "frontity";
import Link from "@frontity/components/link"
import FeaturedMedia from "../featured-media";
import Item from "../posts/item"
import arrow from "../../assets/img/icons/arrow.svg";

const Newses = ({ state, actions }) => {
  const data = state.source.get('/latest-news')
  let i = 0;
  let j = 0;

  return (
    <section className="pt-5 bg-white">
      <div className="container-fluid pt-3 pt-md-5">
        <div className="row">
          <h2 className="col-md-8 ps-md-2 offset-md-2 fw-bold mb-5 pb-md-5" data-aos="fade">Latest news</h2>
          {data.isReady && (
          <div className="col-md-10 pe-60 ps-md-2 offset-md-2">
            <div className="row">
              {data.items.slice(0, 2).map(({ type, id }) => {
                const item = state.source[type][id];
                let className = ''
                if (j == 0) {
                  if (i % 2 == 0) {
                    className = 'col-md-5'
                  } else {
                    className = 'col-md-5 offset-md-1'
                    j = 1;
                  }                  
                } else {
                  if (i % 2 == 0) {
                    className = 'col-md-5'
                  } else {
                    className = 'col-md-5 offset-md-1'
                    j = 0;
                  } 
                }

                return (
                  <article className={className + ' mb-5 hover-arrow'} key={item.id} data-index={i++} data-aos="fade">
                    <Link link={item.link}>
                      {item.featured_media && (
                        <div className="position-relative overflow-hidden">
                          <FeaturedMedia id={item.featured_media} />
                        </div>
                      )}
                      <Itemh2 className="px-2" dangerouslySetInnerHTML={{ __html: item.title.rendered }} />
                      <span className="overflow-hidden px-2 text-blue d-flex align-items-center arrow-wrapper d-inline-block">
                        read more
                        <img src={arrow} alt="arrow"  className="arrow ms-3" />
                      </span>
                    </Link>

                  </article>
                )
              })}                
            </div>          
          </div>
          )}
        </div>
      </div>
    </section>
  )
}

export default connect(Newses)



const Itemh2 = styled.h2`
  font-size: 1.875rem;
  padding-top: 1.25rem;
`
