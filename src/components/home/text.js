import { styled } from "frontity";

const Text = (props) => {
  return (
    <section id="section0" className="pt-5 bg-white">
      <div className="container-fluid pt-4">
        <div className="row">
          <h2 className="col-md-8 offset-md-2 mb-5 h2-big" data-aos="fade" dangerouslySetInnerHTML={{ __html: props.section.title }} />
          <Paragraph className="col-md-8 pe-xl-5 me-xl-5 offset-md-2 paragraph mb-5 pb-2 pe-60" data-aos="fade" data-aos-delay="200" dangerouslySetInnerHTML={{ __html: props.section.text }} />          
        </div>          
      </div>  
    </section>
  );
}

export default Text;

const Paragraph = styled.div`
  p {
    font-size: 1.125rem;
    margin-bottom: 2.25rem;
    line-height: 1.55;

    &:first-of-type {
      font-size: 1.5rem;
      line-height: 1.3;
    }
  }
`