import SwiperCore, { Scrollbar } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { matchHeight } from "../helpers/matchHeight"
import Link from "@frontity/components/link";
import arrow from "../../assets/img/icons/arrow.svg";

SwiperCore.use([Scrollbar]);

const Slider = (props) => {
  const section = props.section;
  let className = 'overflow-hidden bg-dark py-5 text-white';
  let containerClass = 'col-md-9 offset-md-2 paragraph fs-20 text-white mb-5 pb-5 pe-md-4';

  if (props.width) {
    className = 'overflow-hidden bg-dark py-5 text-white full-width';
    containerClass = 'col-md-8 offset-md-4 paragraph fs-20 text-white mb-5 pb-5 pe-md-4';
  }

  return (  
    <section className={className}>
       <div className="container-fluid my-3">
        <div className="row">
          {section.text && (
            <>
              <div className={containerClass} data-aos="fade" dangerouslySetInnerHTML={{ __html: section.text }} />
            </>
          )}

          {section.slider && (
            <>
              <Swiper className="col-12 home-slider pb-5"
                spaceBetween={50}
                slidesPerView="auto"
                onSwiper={(s) => {
                  setTimeout(() => {
                    matchHeight()
                  }, 100)
                }}
                scrollbar={{ draggable: true }}
                data-aos="fade" data-aos-delay="200"
              >
                {section.slider.map((slide, i) => {
                  const title = slide.title;
                  const text = slide.text;
                  const timing = slide.timing;
                  const link = slide.link.replace('https://oyster-kuniejm.vercel.app', '');
                  return (
                    <SwiperSlide className="d-flex flex-column" key={i}>
                      <div>
                        <h3 className="fw-bold mb-4 pb-1" data-h="title">{title}</h3>
                        {timing && (
                          <h3 className="fs-19 text-uppercase mb-4 pb-1 text-blue">{timing}</h3>
                        )}
                        <div className="paragraph lh-1-3 fs-19" dangerouslySetInnerHTML={{ __html: slide.text }} />
                      </div>
                      {link && (
                         <Link link={link} className="hover-arrow mt-auto fw-medium overflow-hidden arrow-wrapper d-inline-flex align-items-center text-blue">
                          read more
                          <img src={arrow} alt="arrow"  className="arrow ms-3" />
                        </Link>
                      )}
                    </SwiperSlide>
                  )
                })}
              </Swiper>
            </>
          )}
        </div>
      </div>
    </section>
  );
}

export default Slider;