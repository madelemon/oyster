import SwiperCore, { Scrollbar, Pagination, EffectFade, Autoplay, Controller } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import gsap from "gsap";
import Link from "@frontity/components/link";
import { connect, styled } from "frontity"
import arrow from "../../assets/img/icons/arrow.svg";
import SvgLine from "../../assets/img/svg/line"; 

SwiperCore.use([Scrollbar, Pagination, EffectFade, Autoplay, Controller]);

const SliderCircles = (props) => {
  const section = props.section;
  let pathProgress = 0;


  const onSlideChange = () => {
    let title = document.querySelector('.home-slider-circles-title');
    let active;
    pathProgress = document.querySelector('.path');
    if (pathProgress) {
      pathProgress.removeAttribute("style");
      gsap.fromTo(pathProgress, 5.4, {strokeDashoffset: -1080}, {strokeDashoffset: 0});      
    }

    gsap.to(title, .1, {opacity: 0, onComplete: () => {
      active = document.querySelector('.home-slider-circles .swiper-slide-active'); 
      if (active !== null) {
        title.innerText = active.getAttribute('data-title');
        gsap.to(title, .3, {opacity: 1});        
      }
    }});  
  }

  return (  
    <SectionSliderCircles className="overflow-hidden bg-white">
       <CircleMedium />
       <div className="container-fluid my-3">
        <div className="row">
          <SvgContainer><SvgLine /></SvgContainer>
          <h2 className="col-md-3 offset-md-2 mb-md-5 text-md-end h2-medium home-slider-circles-title" data-aos="fade"  dangerouslySetInnerHTML={{ __html: section.slider[0].title }} />

          {section.slider && (
            <SwiperHomeCircles className="col-md-6 col-lg-4 position-relative mt-md-5" data-aos="fade" data-aos-delay="200">
              <CircleSmall />
              <Swiper className="mt-5 position-static home-slider-circles pb-5"
                effect={'fade'}
                slidesPerView={1}
                loop={true}
                speed={400}
                autoplay={{
                    delay: 5000,
                    disableOnInteraction: false
                }}
                pagination={{ clickable: true }}
                onSwiper={onSlideChange}
                onSlideChange={onSlideChange}
              >
                {section.slider.map((slide, i) => {
                  const text = slide.text;
                  return (
                    <SwiperSlide key={i*5} data-title={slide.title}>
                      <div className="paragraph fs-19" dangerouslySetInnerHTML={{ __html: slide.text }} />
                    </SwiperSlide>
                  )
                })}
              </Swiper>
            </SwiperHomeCircles>
          )}
        </div>
      </div>
    </SectionSliderCircles>
  );
}

export default SliderCircles;


const SvgContainer = styled.div`
  position: absolute;
  top: 50%;
  right: 20%;
  width: auto;
  transform: translateY(-50%);

  @media screen and (max-width: 992px) {
    right: .625rem;
    left: .625rem;
  }

  svg {
    width: 100%;

    .path {
      stroke-dasharray: 1080;
      stroke-dashoffset: -1080;
    }
  }
`

const SectionSliderCircles = styled.section`
  position: relative;  
  padding-top: 12.5rem;
  padding-bottom: 16.25rem;
`

const SwiperHomeCircles = styled.div`
  padding-left: calc(8.33333333%  + .875rem);

  @media screen and (max-width: 992px) {
    padding-left: .625rem;
    padding-right: 3rem;
  }

  .swiper-pagination {
    position: absolute;
    top: 4.625rem;
    left: 0;
    bottom: auto !important;
    width: auto !important;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    transform: translateX(calc(-100% - .875rem));

    @media screen and (max-width: 992px) {
      position: static;
      align-items: flex-start;
      transform: translateX(0);
      margin-bottom: 3rem;
    }

    &-bullet {
      margin-bottom: .875rem !important;
      height: .25rem;
      width: 4.5rem;
      opacity: 1;
      border-radius: 0;
      background: var(--bs-dark);
      transition: all .3s ease-in-out;

      &:hover {
        background: var(--bs-blue);
      }

      &-active {
        background: var(--bs-blue);
        width: 9.625rem;
      }
    }
  }

  .swiper-container {
    overflow: visible;

    .swiper-slide {
      width: 100% !important;
      opacity: 0 !important;

      .paragraph {
        * {
          font-family: 'CeraPro-Light' !important;
        }

        p {
          margin-bottm: 1rem;
        }
        
      }

      &.swiper-slide-active {
        opacity: 1 !important;
      }
    }
  }
`

const CircleMedium = styled.div`
  width: 12.875rem;
  height: 12.875rem;
  position: absolute;
  bottom: 7.25rem;
  border-radius: 50%;
  border: 2px solid #0d2851;

  @media screen and (min-width: 993px) {
    right: -3%;
  }

  @media screen and (max-width: 992px) {
    left: -5%;
    bottom: 3rem;
  }

  @media screen and (max-width: 575px) {
    left: 50%;
    bottom: 3rem;
    width: 6.875rem;
    height: 6.875rem;
  }
`

const CircleSmall = styled.div`
  width: 2.5rem;
  height: 2.5rem;
  position: absolute;
  right: 6.25rem;
  top: -6.25rem;
  border-radius: 50%;
  border: 2px solid #0d2851;

  @media screen and (max-width: 575px) {
    display: none;
  }
`