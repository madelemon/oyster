import { styled } from "frontity"

const LayoutLines = (props) => {
  return (
    <Lines className="lines" data-aos="draw-lines"><span />{props.color}<span /><span /></Lines>
  );
}

export default LayoutLines;


const Lines = styled.div` 
  position: fixed;
  pointer-events: none;
  top: 0;
  left: 0; 
  right: 0;
  bottom: 0;
  z-index: 2;
  opacity: .3;
  mix-blend-mode: multiply;

  span {
    position: absolute;
    height: 100%;
    width: 1px;
    background: #c9c9c9;

    &:first-of-type {
      left: calc(16.66666667% + 1.875rem - 10px);
      
      @media screen and (max-width: 575px) {
        left: var(--bs-gutter-x,.75rem);
      }
    }

    &:nth-of-type(2) {
      left: calc(50% + 1.875rem - 1.5rem);

      @media screen and (max-width: 575px) {
        left: 50%;
      }
    }

    &:last-of-type {
      right: 4.9rem;
    }
  }
`