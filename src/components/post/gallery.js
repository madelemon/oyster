const Gallery = (props) => {
  const length = props.section.images.length;
  let name = '';
  return (
    <>
      {props.section.images.length == 1 ? (
        <div className="col-md-7 offset-md-4">
          <div className="col-12 mb-5" data-aos="fade"> 
            <img src={props.section.images[0].url}  alt={props.section.images[0].alt} />
          </div>
        </div>
      ) : (
        <div className="row gallery-wrapper">

          {props.section.images.map((img, i) => {
            if (i %2 === 0) { 
              name = 'gallery col-6 col-md-4 offset-md-2 mb-5';
            } else {
              name = 'gallery col-6 col-md-6 mb-5 ps-18 pe-37';
            }
            return (
              <div className={name} key={i} data-aos="fade">
                <img className="img-fit" src={img.url} alt={img.alt} />
              </div>
            )
          })
        }  
        </div>
      )}
    </>
  )
}

export default Gallery;