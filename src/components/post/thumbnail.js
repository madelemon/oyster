import { styled } from "frontity";
import FeaturedMedia from "../featured-media";

const Thumbnail = (props) => {

  return (
    <>
      {props.media && (
        <MediaContainer className="col-md-5 offset-md-4 py-4 mb-5 pe-60">
          <FeaturedMedia id={props.media} />
        </MediaContainer>
      )}
    </>

  );
}

export default Thumbnail;

const MediaContainer = styled.div`
  img {
    max-width: 300px;

    @media screen and (max-width: 375px) {
      max-width: 100%;
    }
  }
`