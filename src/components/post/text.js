const Text = (props) => {
  return (
    <div className="col-md-7 offset-md-4 paragraph mb-5 pb-5 pe-60 entry" data-aos="fade" dangerouslySetInnerHTML={{ __html: props.section.text }} />
  );
}

export default Text;