import React from "react"
import { connect, styled } from "frontity"

const Socials = ({ state, section }) => {
  const options = state.source.get("acf-options-page");
  const data = state.source.get(state.router.link)
  const socials = options.acf.socials || [];
  let liClassName = '';
  let ulClassName = '';

  if (section === 'header') {
    liClassName = 'mx-0 px-0';
    ulClassName = 'position-absolute';

    if (data.isPage || (data.isPostType && !data.isPartner)) {
      liClassName = 'mx-0 px-0 is-white';
    }
  }

  return (
    <>
      {socials && (
         <SocialsUl className={ulClassName}>
          {socials.map((row, i) => {
            return <li key={i} className={liClassName}><a href={row.link} target="_blank" title={row.title}><img src={row.icon} alt={row.title} /></a></li>
          })}  
         </SocialsUl>
      )}
    </>

  )
}

export default connect(Socials)


const SocialsUl= styled.ul`
  list-style-type: none;
  padding-left: 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex-wrap: wrap;

  &.position-absolute {
    margin: auto;
    left: .5rem;
    right: 0;
    top: 5.2rem;
    flex-direction: column;

    img {
      filter: invert(61%) sepia(7%) saturate(2839%) hue-rotate(138deg) brightness(95%) contrast(95%);
    }
  }

  @media screen and (max-width: 1199px) {
    justify-content: flex-start;
  }

  img {
    height: 1rem;
    filter: invert(26%) sepia(27%) saturate(1345%) hue-rotate(158deg) brightness(96%) contrast(93%);
    transition: transform .3s ease-in-out;
  }

  li {
    margin-left: 2.25rem;
    margin-bottom: 2.25rem;

    &.is-white {
      img {
        filter: brightness(0) invert(1);
      }
    }

    @media screen and (max-width: 1199px) {
      margin-left: 0;
      margin-right: 2.25rem;
    }
  }

  a:hover {
    img {
      transform: translateY(.3rem);
    }
  }
`