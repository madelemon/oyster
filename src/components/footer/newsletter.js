import React from "react"
import { connect, styled } from "frontity"
import plus from "../../assets/img/icons/plus.svg";

const Newsletter = ({ state }) => {
  const options = state.source.get("acf-options-page");
  const newsletter = options.acf.newsletter || [];
  const data = state.source.get(state.router.link)

  return (
     <form action="">
        {newsletter && (
          <div className="fs-14 mb-5 lh-1-5 fw-light">{newsletter}</div>
        )}
        <div className="position-relative">
          <Input name="email" type="email" placeholder="your email" />
          <Submit type="submit" value="" />
        </div>
     </form>
  )
}

export default connect(Newsletter)

const Input = styled.input`
  padding: 1rem 0;
  width: 100%;
  border: none;
  background: transparent;
  font-family: 'CeraPro-Light';
  padding-right: 3.125rem;
  border-bottom: 1px solid rgba(23, 58, 121, .3);

  ::-webkit-input-placeholder { 
    color: var(--bs-dark);
  }

  &:focus {
    outline: 0;
  }
`

const Submit = styled.input`
  width: 1.75rem;
  height: 3.125rem;
  position: absolute;
  right: 0;
  top: 0;
  border: none;
  background-color: transparent;
  background-size: 1.75rem 1.75rem;
  background-repeat: no-repeat;
  background-position: center center;
  transform-origin: center center;
  background-image: url(${plus});
  transition: transform .5s cubic-bezier(.7,0,.3,1);

  &:hover {
    transform: rotate(180deg);
  }
`