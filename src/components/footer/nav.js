import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"
import Socials from './socials'

const FooterNav = ({ state }) => {

  return (
    <div className="d-flex justify-content-between flex-wrap mb-md-5 pb-lg-5">
      <ul className="ps-0 col-12 col-md-6 col-xl-4 mb-0 list-unstyled text-uppercase">
        <li><Link link="about/context">About</Link></li>
        <li><Link link="partner">Partners</Link></li>
      </ul>
      <ul className="ps-0 col-12 col-md-6 col-xl-4 mb-0 list-unstyled text-uppercase">
        <li><Link link="latest-news">News</Link></li>
        <li><Link link="faq">FAQ</Link></li>
        <li><Link link="contact">Contact</Link></li>
      </ul>
      <div className="pb-lg-5 pt-5 pt-xl-0 col-lg-12 col-xl-4">
        <Socials section="footer" />
      </div>
    </div>
  )
}

export default connect(FooterNav)
