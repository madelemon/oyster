import React from "react"
import { connect, styled } from "frontity"

const Logos = ({ state }) => {
  const options = state.source.get("acf-options-page");
  const logos = options.acf.logos || [];
  const logos_text = options.acf.logos_text || [];

  return (
    <div>
       <ul className="d-flex align-items-center ps-0 mb-0">
        {logos.map((logo, i) => {
          return <Logo href={logo.caption} className="d-inline-block me-5 mb-5" target="_blank" key={i}><img className="me-lg-3" src={logo.url} alt={logo.alt} /></Logo>
        })}  
       </ul>
       <Paragraph dangerouslySetInnerHTML={{ __html: logos_text }} />
    </div>
  )
}

export default connect(Logos)


const Paragraph = styled.div`
  font-size: .75rem;
  opacity: .6;
  line-height: 1.4;

  * {
    font-family: 'CeraPro-Light';
  }
`

const Logo = styled.a`
  img {
    min-width: 6rem;
  }
`