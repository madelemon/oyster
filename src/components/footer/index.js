import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"
import Logos from './logos'
import FooterNav from './nav'
import Newsletter from './newsletter'
import logo from "../../assets/img/static/oyster-logo-color.gif";


const Footer = ({ state }) => {
  const data = state.source.get(state.router.link)
  const options = state.source.get("acf-options-page");

  return (
    <FooterEl>
      <div className="container-fluid">
        <div className="row" data-aos="fade" data-aos-anchor="footer">
          <div className="col-md-4">
            <Link link="/" className="logo d-inline-block mb-5"><img className="mb-lg-3" src={logo} alt="Oyster" width="215" height="98"/></Link>
            <Logos />
          </div>
          <div className="col-md-6 offset-md-2 mt-4 mt-md-0">
            <FooterNav />
            <Newsletter />
          </div>
        </div>
      </div>
    </FooterEl>
  )
}

export default connect(Footer)

const FooterEl = styled.footer`
  padding: 4.8rem 0 3.5rem;
  position: relative;
  z-index: 4;
  background: var(--bs-light);

  a:hover {
    opacity: .85;
  }

  li > a {
    letter-spacing: .5px;

    &:hover {
      color: var(--bs-blue);
    }
  }
`

