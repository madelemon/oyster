import React, { useEffect } from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link";
import FeaturedMedia from "./featured-media";
import Text from "./post/text";
import Gallery from "./post/gallery";
import Thumbnail from "./post/thumbnail";
import ListPartner from "./list-partner";
import arrow from "../assets/img/icons/arrow.svg";
import Header from "./header/index";
import Footer from "./footer/index";

const Partner = ({ state, actions, libraries }) => {
  // Get information about the current URL.
  const data = state.source.get(state.router.link);
  // Get the data of the post.
  const post = state.source["partner"][data.id];
  const excerpt = post.excerpt;
  const sections = post.acf.sections;
  let classname = "";

  // Get the html2react component.
  const Html2React = libraries.html2react.Component;
  const dataPartner = state.source.get("/partner");

  // useEffect(() => {
  //   await actions.source.fetch('/partner');
  //   const dataPartner = state.source.get("/partner");
  // }, []);

  // Load the post, but only if the data is ready.
  return data.isReady ? (
    <>
    <Header />
    <section className="pt-30vh">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4 mb-5 mb-md-0">
            <PartnerUl className="fs-20 lh-1-5">
              <li>
                <a href="../../partner">Partners</a>
                {dataPartner.isReady && (
                  <ul className="list-unstyled">
                    {dataPartner.items.map(({ type, id }, index) => {
                      const item = state.source.partner[id];
                      if (state.router.link == item.link) {
                        classname = "current-menu-item";
                      } else {
                        classname = "";
                      }
                      return (
                        <li key={item.id} className={classname}>
                          <Link link={item.link}>{item.title.rendered}</Link>
                        </li>
                      );
                    })}
                  </ul>
                )}
              </li>
            </PartnerUl>
          </div>
          <div className="col-md-7 mb-5 pe-60">
            <h1 className="mb-4 mb-sm-5" dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
            {post.excerpt && (
              <Excerpt
                className="lh-1-3"
                dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }}
              />
            )}
          </div>

          {sections && (
            <>
              {sections.map((section, i) => {
                if (section.acf_fc_layout === "text")
                  return <Text section={section} key={i} />;

                if (section.acf_fc_layout === "gallery")
                  return <Gallery section={section} key={i} />;

                if (section.acf_fc_layout === "thumbnail")
                    return <Thumbnail media={post.featured_media} key={i} />;
              })}
            </>
          )}
          {dataPartner.isReady && (
            <div className="my-5 py-5">
              <div className="col-12 d-flex ps-58 flex-wrap">
                {dataPartner.items.map(({ type, id }, index) => {
                  const item = state.source.partner[id];
                  if (state.router.link == item.link) {
                    classname = "is-active";
                  } else {
                    classname = "";
                  }
                  return (
                    <PartnerItem key={item.id} className={classname}>
                      <Link
                        key={item.id}
                        link={item.link}
                        className="hover-arrow"
                      >
                        {state.source.partner[id].featured_media && (
                          <FeaturedMedia
                            id={state.source.partner[id].featured_media}
                          />
                        )}
                        <span className="overflow-hidden arrow-wrapper d-inline-block">
                          <img src={arrow} alt="arrow" className="arrow" />
                        </span>
                      </Link>
                    </PartnerItem>
                  );
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    </section>
    <Footer />
    </>
  ) : null;
};



export default connect(Partner);


const PartnerUl = styled.ul`
  list-style-type: none;
  text-transform: uppercase;
  padding-left: 0;
  margin-bottom: 0;

  a:hover {
    color: var(--bs-blue);
  }

  ul {
    a {
      position: relative;
      padding-left: 0;

      &:before {
        content: '—';
        position: absolute;
        left: 0;
        top: 50%;
        opacity: 0;
        color: var(--bs-blue);
        transform: translateY(-50%);
        transition: all .35s ease-in-out;
      }
    }

    .current-menu-item > a, 
    a:hover {
      color: var(--bs-blue);
      padding-left: 1.625rem;

      &:before {
        opacity: 1;
      }
    }    
  }


`

const Excerpt = styled.div`
  font-size: 1.5rem;

  @media screen and (max-width: 992px) {
    font-size: 1.125rem;
  }

  *:last-child {
    margin-bottom: 0;
  }
`


const PartnerItem = styled.div`
  width: calc(25% - 12px);
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all .3s ease-in-out;

  @media screen and (max-width: 992px) {
    width: calc(50% - 24px);
  }

  @media screen and (max-width: 575px) {
    width: 50%;
  }

  a {
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    background: var(--bs-light);
    padding: 2.5rem;
    position: relative;

    .arrow-wrapper {
      position: absolute;
      right: 1.125rem;
      bottom: 1.125rem;
    }
  }

  &.is-active a,
  &:hover a {
    background-color: var(--bs-white);
  }

  img {
    max-height: 2.875rem;
    width: auto;
  }
}
`


