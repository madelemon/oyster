import React, { useState } from "react";
import { Accordion, Card, Button } from "react-bootstrap";

const AccordionsSection = (props) => {
  const index = props.index ? props.index : 0;
  const [currentActiveKey, setCurrentActiveKey] = useState(null);

  const toggleActiveKey = (key) => {
    setCurrentActiveKey(currentActiveKey === key ? 0 : key);
  };


  return (
    <>
      {props.section.accordions.length && (
        <Accordion className="pe-md-4 mb-4 pb-5" defaultActiveKey={index + 1}>

          {props.section.accordions.map((el, i) => {
            return (
              <Card key={index + i + 1} className={i === 0 ? "is-active" : ""}>
                <Accordion.Toggle as={Button} eventKey={index + i + 1} onClick={() => { toggleActiveKey(index + i + 1);}} data-aos="fade">
                  {el.title}
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={index + i + 1}>
                  <Card.Body className="paragraph" dangerouslySetInnerHTML={{ __html: el.text }} />
                </Accordion.Collapse>
              </Card>
            )
          })
        }  
        </Accordion>
      )}
    </>
  )
}

export default AccordionsSection;
