import { styled } from "frontity";

const Img = (props) => {
  return (
    <ImgContainer className="w-100" data-aos="fade">
      <img src={props.section.img} alt="" />
    </ImgContainer>
  );
}

export default Img;

const ImgContainer = styled.div`
  width: 100vw;
  margin-top: -3rem;
  margin-left: -1.25rem;

  @media (min-width: 575px) {
    margin-left: -1.875rem;
  }

  @media (min-width: 768px) {
    margin-left: calc(-33vw - 1.875rem);
  }

  img {
    max-width: 100vw;
  }
`