import React from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link";
//import FeaturedMedia from "../featured-media";
import Text from "../page/text";
import Img from "../page/img";
import Slider from "../home/slider";
import AccordionsSection from "../page/accordions";

const Content = ({ state, actions, libraries }) => {
  // Get information about the current URL.
  const data = state.source.get(state.router.link);
  // Get the data of the post.
  const post = state.source[data.type][data.id];
  const sections = post.acf.sections;
  const Html2React = libraries.html2react.Component;

  return data.isReady ? (
    <>

          {sections && (
            <>
              {sections.map((section, i) => {
                if (section.acf_fc_layout === "text")
                  return <Text section={section} index={i} key={i} />;

                if (section.acf_fc_layout === "img")
                  return <Img section={section} index={i} key={i} />;

                if (section.acf_fc_layout === "accordions")
                  return <AccordionsSection section={section} index={i} key={i} />;

                if (section.acf_fc_layout === "section_slider")
                  return <Slider section={section} index={i} key={i} width="100" />;
              })}
            </>
          )}
    </>
  ) : null;
};



export default connect(Content);


const ContentUl = styled.ul`
  list-style-type: none;
  text-transform: uppercase;
  padding-left: 0;
  margin-bottom: 0;

  a:hover {
    color: var(--bs-blue);
  }

  ul {
    a {
      position: relative;
      padding-left: 0;

      &:before {
        content: '—';
        position: absolute;
        left: 0;
        top: 50%;
        opacity: 0;
        color: var(--bs-blue);
        transform: translateY(-50%);
        transition: all .35s ease-in-out;
      }
    }

    .current-menu-item > a, 
    a:hover {
      color: var(--bs-blue);
      padding-left: 1.625rem;

      &:before {
        opacity: 1;
      }
    }    
  }
`
