import React, { useRef } from "react"
import { connect, styled } from "frontity"
//import FeaturedMedia from "../featured-media";

const HeroPage = ({ state }) => {
  const options = state.source.get("acf-options-page");
  const data = state.source.get(state.router.link)
  const page = state.source[data.type][data.id]
  const title = page.title.rendered;
  const videos = options.acf.videos || [];
  const videoParentRef = useRef();
  let videoUrl = null;

  if (videos !== undefined) {
    videoUrl = videos[Math.floor(Math.random() * videos.length)];
  }

  return (
    <Hero>
      <BgLayer />
      {videoUrl && (
        <Video className="pin-video"
          ref={videoParentRef}
          dangerouslySetInnerHTML={{
            __html: `
            <video
              loop
              muted
              autoplay
              playsinline
              src="${videoUrl}"
            >
            </video>`
          }} />
      )}
      {/*state.source.[data.type][data.id].featured_media && (
        <FeaturedMedia id={state.source.[data.type][data.id].featured_media} />
      )*/}

      <div className="container-fluid text-white position-relative">
        <div className="row align-items-center">
          {data.type !== "post" && (
            <h1 className="col-md-7 offset-md-4" data-aos="fade">{title}</h1>
          )}
        </div>
      </div>
    </Hero>
  )
}

export default connect(HeroPage)

const Hero = styled.section`
  background-color: var(--bs-dark);
  min-height: 25.625rem;
  display: flex;
  align-items: flex-end;
  padding-bottom: 2.5rem;
  position: relative;
  overflow: hidden;

  .container-fluid {
    z-index: 4;
  }

  h1 {
    margin-bottom: 0;
  }

  img {
    height: 100%;
    left: 0;
    top: 0;
    bottom: 0;
    opacity: .5;
    width: 100%;
    position: absolute;
    object-fit: cover;
  }
`

export const BgLayer = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: -10rem;
  background: var(--bs-dark);
  opacity: .5;
  z-index: 2;
`

const Video = styled.div`
  margin-top: -10rem;

  video {
    pointer-events: none;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    object-fit: cover;
    object-position: 50% 50%;
  }
`