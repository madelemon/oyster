const Text = (props) => {
  return (
    <div className="paragraph entry mb-5 pb-5 pe-md-4" data-aos="fade" dangerouslySetInnerHTML={{ __html: props.section.text }} />
  );
}

export default Text;