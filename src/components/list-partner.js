import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"
import FeaturedMedia from "./featured-media";
import arrow from "../assets/img/icons/arrow.svg";
import Header from "./header/index";
import Footer from "./footer/index";

const ListPartner = ({ state }) => {
  const data = state.source.get(state.router.link);
  const options = state.source.get("acf-options-page");
  const partners_text = options.acf.partners_text || '';

  return data.isReady ? (
    <>
    <Header />
    <section className="pt-30vh">
      <div className="container-fluid mb-5 pb-5">
        <div className="row"> 
          <div className="col-md-4 mb-5 mb-md-0">
            <PartnerUl className="fs-20 lh-1-5">
              <li className="current-menu-item">
                <a href="../../partner">Partners</a>
                {data.isReady && (
                  <ul className="list-unstyled">
                    {data.items.map(({ type, id }, index) => {
                      const item = state.source.partner[id];
                      return (
                        <li key={item.id}>
                          <Link link={item.link}>{item.title.rendered}</Link>
                        </li>
                      );
                    })}
                  </ul>
                )}
              </li>
            </PartnerUl>
          </div>
          {partners_text && (
            <div className="col-md-7 paragraph mb-5 pb-5 pe-60" data-aos="fade">

              <h1 data-aos="fade">Partners</h1>
              <div className="paragraph" dangerouslySetInnerHTML={{ __html: partners_text }} />
            </div>
          )}
          <div className="col-12">
            <div className="col-12 d-flex ps-58 flex-wrap">
              {data.items.map(({ type, id }) => {
                const item = state.source.partner[id];
                return (
                  <PartnerItem key={item.id}>
                    <Link key={item.id} link={item.link} className="hover-arrow">
                    {state.source.partner[id].featured_media && (
                      <FeaturedMedia id={state.source.partner[id].featured_media} />
                    )}
                      <span className="overflow-hidden arrow-wrapper d-inline-block">
                        <img src={arrow} alt="arrow"  className="arrow" />
                      </span>
                    </Link>
                  </PartnerItem>
                )
              })}
            </div>   
          </div>       
        </div>
      </div>
    </section>
    <Footer />
    </>
  ) : null 
}

export default connect(ListPartner)


const PartnerUl = styled.ul`
  list-style-type: none;
  text-transform: uppercase;
  padding-left: 0;
  margin-bottom: 0;

  a:hover,
  .current-menu-item > a {
    color: var(--bs-blue);
  }

  ul {
    a {
      position: relative;
      padding-left: 0;

      &:before {
        content: '—';
        position: absolute;
        left: 0;
        top: 50%;
        opacity: 0;
        color: var(--bs-blue);
        transform: translateY(-50%);
        transition: all .35s ease-in-out;
      }
    }

    .current-menu-item > a, 
    a:hover {
      color: var(--bs-blue);
      padding-left: 1.625rem;

      &:before {
        opacity: 1;
      }
    }    
  }
`


const PartnerItem = styled.div`
  display: flex;
  align-items: center;
  width: calc(25% - 12px);
  justify-content: center;
  transition: all .3s ease-in-out;

  @media screen and (max-width: 992px) {
    width: calc(50% - 24px);
  }

  @media screen and (max-width: 575px) {
    width: 50%;
  }

  a {
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    background: var(--bs-light);
    padding: 2.5rem;
    position: relative;

    .arrow-wrapper {
      position: absolute;
      right: 1.125rem;
      bottom: 1.125rem;
    }
  }


  &.is-active a,
  &:hover a {
    background-color: var(--bs-white);
  }

  img {
    max-height: 2.875rem;
    width: auto;
  }
}
`
