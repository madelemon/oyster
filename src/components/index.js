import React, { useEffect } from 'react';
import { useTransition, animated } from 'react-spring';
import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import { Head, connect, Global, css, styled } from "frontity"
import AOS from "aos";
import Link from "@frontity/components/link"
import Switch from "@frontity/components/switch"
import ListPartner from "./list-partner";
import Posts from "./posts";
import Partner from "./partner";
import Page from "./page";
import Post from "./post";
import Home from "./home";
import Title from "./title";
import Header from "./header/index";
import Footer from "./footer/index";


// imgs
import fav16 from "../assets/img/favicons/favicon-16x16.png";
import fav32 from "../assets/img/favicons/favicon-32x32.png";
import windmill from "../assets/img/static/loader.png";
import logoW from "../assets/img/static/oyster-logo-white.gif";

// fonts;
import ceraProBlackEot from "../assets/fonts/CeraPro-Black.eot";
import ceraProBlackTtf from "../assets/fonts/CeraPro-Black.ttf";
import ceraProBlackWoff from "../assets/fonts/CeraPro-Black.woff";
import ceraProBlackWoff2 from "../assets/fonts/CeraPro-Black.woff2";;
import ceraProBoldEot from "../assets/fonts/CeraPro-Bold.eot";
import ceraProBoldTtf from "../assets/fonts/CeraPro-Bold.ttf";
import ceraProBoldWoff from "../assets/fonts/CeraPro-Bold.woff";
import ceraProBoldWoff2 from "../assets/fonts/CeraPro-Bold.woff2";
import ceraProMediumEot from "../assets/fonts/CeraPro-Medium.eot";
import ceraProMediumTtf from "../assets/fonts/CeraPro-Medium.ttf";
import ceraProMediumWoff from "../assets/fonts/CeraPro-Medium.woff";
import ceraProMediumWoff2 from "../assets/fonts/CeraPro-Medium.woff2"
import ceraProLightEot from "../assets/fonts/CeraPro-Light.eot";
import ceraProLightTtf from "../assets/fonts/CeraPro-Light.ttf";
import ceraProLightWoff from "../assets/fonts/CeraPro-Light.woff";
import ceraProLightWoff2 from "../assets/fonts/CeraPro-Light.woff2";

const Root = ({ state }) => {
  const data = state.source.get(state.router.link)
  const options = state.source.get("acf-options-page")
  const meta_title = options.acf.meta_title || '';
  const meta_desc = options.acf.meta_desc || '';
  const meta_img = options.acf.meta_img || '';
  let metaPageTitle = '';
  if (state.source[data.type] !== undefined) {
    const page = state.source[data.type][data.id];
    if (page !== undefined) {
      metaPageTitle = page.title.rendered + ' - ' + meta_title;
    }
  }
  

  const transitions = useTransition(state.router.link, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0, display: "none" },
    delay: 200,
    onRest: () => {
      state.theme.isMobileMenuOpen = false;
      AOS.init({
       once: true,
       duration: 800,
       easing: 'ease-out-sine'
      });
      AOS.refresh();
    }
  });

  const transitionLoader = useTransition(state.router.link, {
    from: { opacity: 1 },
    enter: { opacity: 0 },
    leave: { opacity: 1, display: "none" },
  });


  return (
    <>      
      <Title />
      <Head>
        <meta name="description" content={state.frontity.description} />
        <html lang="en" />
        <link rel="icon" type="image/png" href={fav32} sizes="32x32" />
        <link rel="icon" type="image/png" href={fav16} sizes="16x16" />
        <meta property="og:locale" content="en_GB" />
        <meta property="og:type" content="website" />
        {data.isHome && (
          <meta property="og:title" content={meta_title} />
        )}
        {!data.isHome && (
          <meta property="og:title" content={metaPageTitle} />
        )}
        <meta property="og:description" content={meta_desc} />
        <meta property="og:image" content={meta_img} />
      </Head>
      <Global styles={styleWithFont} />
      <Global styles={bootstrap} />
      <Global styles={globalStyles} />
      <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />


      {transitionLoader((props, item, key) => (
        <animated.div style={props} key={key} className={(state.router.link === "/latest-news/" || data.isCategory) ? 'loader is-hidden' : 'loader'}><img src={windmill} alt="loader" /></animated.div>
      ))}    
      {transitions((props, item, key) => (
        <animated.div style={props} key={key} className="page-container">
          <Switch>
            <Home when={data.isHome} />
            <Posts when={(data.isArchive && !data.isPartnerArchive) || data.isCategory} />
            <ListPartner when={data.isPartnerArchive} />
            <Partner when={data.isPartner && data.isPostType} />
            <Post when={data.isPostType && !data.isPage} />
            <Page when={data.isPage} /> 
            {/*<Block><img src={logoW} width="168" height="78" className="img-white"/> Soon</Block>*/}
          </Switch>        
        </animated.div>
      ))}
    </>
  )
}

export default connect(Root)

const globalStyles = css`
  :root {
    --bs-gutter-x: 20px;
    --bs-dark: #0d2851;
    --bs-blue: #058a9c;
    --bs-blue2: #155275;
    --bs-light: #f6f6f6;
  }

  .page-container {
    min-height: 400px;
  }

  * {
    color: var(--primary);
    font-family: 'CeraPro-Medium';
  }

  body {
    font-size: 16px;
    overflow-x: hidden;
    color: var(--bs-dark);
  }

  section {
    position: relative;
    z-index: 2;

    // .lines {
    //   mix-blend-mode: unset;
    //   position: absolute;
    // }
  }

  .container-fluid {
    @media screen and (min-width: 575px) {
      padding-left: 1.875rem;
      padding-right: 1.875rem;
    }
  }

  .gallery {
    max-height: 95vh;

    img {
      max-height: 100%;
    }
  }

  .gallery-wrapper {
    @media screen and (max-width: 575px) {
      max-width: calc(100% + 24px);
      width: auto;    
    }
  }

  a {
    text-decoration: none;
    cursor: pointer;
    color: var(--bs-dark);
    transition: all .3s ease-in-out;

    &:hover,
    &:focus {
      color: currentColor;
    }
  }

  h1 {
    line-height: 1;
    font-size: 4.875rem;
    font-family: 'CeraPro-Bold';
    margin-bottom: 3.125rem;

    @media screen and (max-width: 992px) {
      font-size: 3.5rem;
    }

    @media screen and (max-width: 375px) {
      font-size: 2.4rem;
    }

    @media screen and (max-width: 340px) {
      font-size: 1.8rem;
    }
  }

  h2,
  .h2 {
    font-size: 2.5rem;
    font-family: 'CeraPro-Bold';

    @media screen and (max-width: 375px) {
      font-size: 2rem;
    }

    @media screen and (max-width: 340px) {
      font-size: 1.4rem;
    }

    &-big {
      font-size: 3.75rem;

      @media screen and (max-width: 375px) {
        font-size: 2.8rem;
      }
    }

    &-medium {
      font-size: 5.8rem;

      @media screen and (max-width: 992px) {
        font-size: 4rem;
      }

      @media screen and (max-width: 575px) {
        font-size: 2.8rem;
      }
    }
  }

  .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
    font-weight: normal;
  }

  .paragraph {
    font-size: 1.125rem;
    line-height: 1.7;
    font-family: 'CeraPro-Light';

    h2 {
      font-size: 2rem;
      margin-bottom: 2.5rem;
      font-family: 'CeraPro-Medium';
    }

    p, ul, ol {
      color: var(--bs-dark);
      margin-bottom: 2.5rem;
      font-family: 'CeraPro-Light';

      img {
        width: auto;
        height: auto;
      }
    }

    ul, ol {
      padding-left: 1.25rem;

      li {
        font-family: inherit;
      }
    }

    blockquote {
      margin: 1.875rem 0 1.5rem;

      * {
        color: var(--bs-blue);
      }
    }

    *:last-child {
      margin-bottom: 0;
    }
  }

  img {
    display: block;
    max-width: 100%;
  }

  .img-fit {
    object-fit: cover;
    min-height: 100%;
    width: 100%;
  }

  .fs-19 {
    font-size: 1.1875rem;
  }

  .fs-20 {
    font-size: 1.25rem;
  }

  .lh-1 {
    line-height: 1;
  }

  .lh-1-3 {
    line-height: 1.3;
  }

  .lh-1-5 {
    line-height: 1.5;
  }

  .text-blue {
    color: var(--bs-blue) !important;
  }

  .text-white {
    color: var(--bs-white) !important;

    p:not(.text-blue),
    ul:not(.text-blue),
    li:not(.text-blue),
    ol:not(.text-blue) {
      color: inherit !important;
    }
  }

  .bg-dark {
    background: var(--bs-dark) !important;
  }

  .pt-30vh {
    padding-top: 30vh;
  }

  .pe-37 {
    padding-right: 2.3125rem;

    @media screen and (max-width: 575px) {
      padding-right: 12px;
    }
  }

  .ps-18 {
    padding-left: 1.125rem;
  }

  .ps-58 {
    @media screen and (min-width: 576px) {
      padding-left: 3.625rem;
    }
  }

  .pe-60 {
    padding-right: 3.75rem;

    @media screen and (max-width: 575px) {
      padding-right: 12px;
    }
  }

  .full-width .home-slider {
    .paragraph {
      max-width: 100%;
    }

    @media screen and (min-width: 575px) {
      .swiper-slide {
        width: 55%;
      }
    }

    @media screen and (min-width: 768px) {
      padding-left: calc(33.33% + .875rem) !important;

      .swiper-scrollbar {
        left: calc(33% + .85rem)!important;
        width: calc(66.66666667% + .875rem) !important;
      }
    }    
  }

  .home-slider {
    overflow: visible !important;
    cursor: grab;

    @media screen and (min-width: 768px) {
      padding-left: calc(16.66% + .875rem) !important;
    }


    .swiper-scrollbar {
      bottom: initial;
      width: calc(100vw - 1.25rem) !important;
      left: .75rem !important;
      top: 0;
      height: 1px !important;
      border-radius: 0 !important;
      background-color: rgba(255, 255, 255, .6);

      @media screen and (min-width: 768px) {
        width: calc(100vw - .75rem - 16.666%);
        left: calc(16.66666667% + .75rem)!important;
      }

      .swiper-scrollbar-drag {
        height: .375rem;
        margin-top: -3px;
        border-radius: 0;
        background: var(--bs-blue) !important;
      }
    }

    .swiper-slide {
      padding-top: 3rem;
      margin-top: 3rem;
      padding-left: 3rem;
      width: 70%;

      @media screen and (min-width: 575px) {
        width: 50%;
      }

      @media screen and (min-width: 992px) {
        width: calc(33.33% + 1.5rem);
      }

      &:last-of-type {
        margin-right: 4.375rem !important;
      }

      &:before {
        content: '';
        width: 3.5rem;
        height: 2.5rem;
        position: absolute;
        left: 0;
        top: 0;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: left top;
        background-image: url(${windmill});
      }
    }


    .paragraph {
      max-width: 17.5rem;

      p {
        margin-bottom: 2rem;
      }
    }
  }

  .entry {
    p, ul, ol {
      a:hover {
        color: var(--bs-blue);
      }
    }
  }
  
  .accordion {
    .btn {
      border-radius: 0;
      outline: 0;
      cursor: pointer;
      color: currentColor;
      font-size: 2rem;
      border: none;
      line-height: 1.25;
      padding: 1.125rem 3.75rem 1.125rem 0;
      background: transparent;
      position: relative;
      text-align: left;

      @media screen and (max-width: 992px) {
        font-size: 1.125rem;
      }

      &:hover,
      &:focus {
        box-shadow: none;
        outline: 0;
      }

      &:hover {
        &:before {
          transform: translateY(-50%) rotate(180deg);
        }

        &:after {
          transform: translateY(-50%) rotate(90deg);
        }
      }

      &:before,
      &:after {
        content: '';
        width: 1rem;
        height: .25rem;
        background: currentColor;
        position: absolute;
        right: 1.25rem;
        top: 2.375rem;
        transform: translateY(-50%) rotate(0);
        transition: all .3s ease-in-out;

        @media screen and (max-width: 992px) {
          top: 1.8rem;
        }
      }

      &:before {
        transform: translateY(-50%) rotate(90deg);
      }
    } 

    .card-body {
      padding: 1rem 3.75rem 5.255rem 0;
    }

    .card {
      border: 0;
      border-radius: 0;
      border-bottom: 1px solid #b4dce1;

      &.is-active {
        .btn {
          &:before {
            transform: translateY(-50%) rotate(180deg);
          }
          
          &:hover {
            &:before {
              transform: translateY(-50%) rotate(135deg);
            }

            &:after {
              transform: translateY(-50%) rotate(45deg);
            }
          }
        }
      }
    }
  }

  article a {
    display: block;

    .overflow-hidden img {
      transition: transform 1.4s cubic-bezier(.165,.84,.44,1);
    }

    &:hover {
      .overflow-hidden img {
        transform: scale(1.1);
      }
    }
  }

  b, strong {
    font-weight: normal;
  }

  [data-aos="draw-lines"] {
    transform: scaleY(0);
    transform-origin: center top;
    opacity: 0;

    transition-property: transform, opacity;

    &.aos-animate {
      transform: scaleY(1);
      opacity: .3;
    }
  }

  body,
  .nav-main .container-fluid {
    @media screen and (min-width: 575px) {
      scrollbar-color: transparent;
      &::-webkit-scrollbar {
        width: 3px;
      }
      &::-webkit-scrollbar-track {
        background: var(--bs-white);
      }
      &::-webkit-scrollbar-thumb {
        background-color: var(--bs-blue);
        border-radius: 0;
      } 
    } 
  }

  @keyframes loading {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  .loader {
    position: fixed;
    top: 50%;
    z-index: 999;
    left: 50%;
    pointer-events: none;
    transform: translate(-50%, -50%);
    transition: opacity .3s ease-in-out;

    &.is-hidden {
      opacity: 0 !important;
      z-index: -1;
      display: none !important;
      visibility: hidden;
    }

    img {
      max-height: 2rem;
      transform-origin: center center;
      animation-name: loading;
      animation-duration: 1s;
      animation-iteration-count: infinite;
      animation-timing-function: linear;
    }
  }

  .full-width {
    width: 100vw;
    margin-top: -3rem;
    margin-left: -1.25rem;

    @media (min-width: 575px) {
      margin-left: -1.875rem;
    }

    @media (min-width: 768px) {
      margin-left: calc(-33vw - 1.25rem);
    }
  }


  @keyframes arrow-slide {
    0% {
      transform: translate(0);
      opacity: 1;
    }
    70% {
      transform: translate(40px,-40px);
      opacity: 0;
    }
    71% {
      transform: translate(-40px,40px);
      opacity: 0;
    } 
    100% {
      transform: translate(0); 
      opacity: 1;
    }
  }

  .hover-arrow {
    &:hover img.arrow {
      animation-name: arrow-slide;
      animation-duration: .4s;
      animation-iteration-count: 1;
      animation-timing-function: ease-in-out;
    }
    }
  }

`;


const styleWithFont = css`
  @font-face {
    font-family: 'CeraPro-Black';
    font-style: normal;
    font-weight: normal;

    src: url('${ceraProBlackEot}');
    src: url(${ceraProBlackTtf}) format('truetype'),
       url(${ceraProBlackWoff}) format('woff'),
       url(${ceraProBlackWoff2}) format('woff2');
  }

  @font-face {
    font-family: 'CeraPro-Bold';
    font-style: normal;
    font-weight: normal;

    src: url('${ceraProBoldEot}');
    src: url(${ceraProBoldTtf}) format('truetype'),
       url(${ceraProBoldWoff}) format('woff'),
       url(${ceraProBoldWoff2}) format('woff2');
  }

  @font-face {
    font-family: 'CeraPro-Medium';
    font-style: normal;
    font-weight: normal;

    src: url('${ceraProMediumEot}');
    src: url(${ceraProMediumTtf}) format('truetype'),
       url(${ceraProMediumWoff}) format('woff'),
       url(${ceraProMediumWoff2}) format('woff2');
  }

  @font-face {
    font-family: 'CeraPro-Light';
    font-style: normal;
    font-weight: normal;

    src: url('${ceraProLightEot}');
    src: url(${ceraProLightTtf}) format('truetype'),
       url(${ceraProLightWoff}) format('woff'),
       url(${ceraProLightWoff2}) format('woff2');
  }

  body .fw-light {
    font-weight: normal !important;
    font-family: 'CeraPro-Light';
  }

  body .fw-bold {
    font-weight: normal !important;
    font-family: 'CeraPro-Bold';
  }
`;

export const BgLayer = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: -10rem;
  background: var(--bs-dark)
  opacity: .5;
  z-index: 2;
`

const Block = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  color: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  top: 0;
  background: var(--bs-dark);
  z-index: 99;

  img {
    margin: 0 auto 2rem;
  }
`