import React, {useEffect} from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link"
import FeaturedMedia from "./featured-media";
import Hero from "./page/hero";
import Item from "./posts/item"
import Cats from "./posts/cats";
import arrow from "../assets/img/icons/arrow.svg";
import Header from "./header/index";
import Footer from "./footer/index";

const Posts = ({ state, actions, libraries }) => {
  const data = state.source.get(state.router.link)
  let i = 0;
  let j = 0;

  return data.isReady ? (
    <>
    <Header />
    <section className="pt-30vh">
      <div className="container-fluid">
        <div className="row">  
          <div className="col-md-4 mb-5 mb-md-0">
            <Cats />
          </div>
          
          <div className="col-md-8 mb-5 pe-60">
            <h1 className="mb-4 mb-sm-5">Latest news</h1>
            <div className="row">
              {data.items.map(({ type, id }) => {
                const item = state.source[type][id];
                {data.items.length}
                let className = ''
                if (j == 0) {
                  if (i % 2 == 0) {
                    className = 'col-md-4'
                  } else {
                    className = 'col-md-7 offset-md-1'
                    j = 1;
                  }                  
                } else {
                  if (i % 2 == 0) {
                    className = 'col-md-7'
                  } else {
                    className = 'col-md-4 offset-md-1'
                    j = 0;
                  } 
                }

                return (
                  <article className={className + ' mb-5 hover-arrow'} key={item.id} data-index={i++}>
                    <Link link={item.link}>
                      {item.featured_media && (
                        <div className="position-relative overflow-hidden">
                          <FeaturedMedia id={item.featured_media} />
                        </div>
                      )}
                      <Itemh2 className="px-2" dangerouslySetInnerHTML={{ __html: item.title.rendered }} />
                      <span className="overflow-hidden px-2 text-blue d-flex align-items-center arrow-wrapper d-inline-block">
                        read more
                        <img src={arrow} alt="arrow"  className="arrow ms-3" />
                      </span>
                    </Link>

                  </article>
                )
              })}    
              {!data.items.length && (
                <div>There are no posts.</div>
              )}
            </div>          
          </div>
        </div>
      </div>
    </section>
    <Footer />
    </>
  ) : null 

}

export default connect(Posts);


const Itemh2 = styled.h2`
  font-size: 1.875rem;
  padding-top: 1.25rem;
`
