import React from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link"

const Cats = ({ state, libraries }) => {
  const data = state.source.get(state.router.link)
  const { items } = state.source.data["all-categories/"];

  return (
    <List className="fs-20 lh-1-5">
      <li className={!data.isCategory ? "current-menu-item" : ""}><Link link="/latest-news">All</Link></li>
      {items.map(item => {
        const { name } = item;
        const link = libraries.source.normalize(item.link);
        return (
          <li key={name} className={state.router.link === link ? 'current-menu-item' : ''}>
            <Link title={name} link={link}>{name}</Link>
          </li>
        )
      })}
    </List>
  );
};

export default connect(Cats);

const List = styled.ul`
  list-style-type: none;
  text-transform: uppercase;
  padding-left: 0;
  margin-bottom: 0;

  a:hover,
  .current-menu-item > a {
    color: var(--bs-blue);
  }

  ul {
    a {
      position: relative;
      padding-left: 0;

      &:before {
        content: '—';
        position: absolute;
        left: 0;
        top: 50%;
        opacity: 0;
        color: var(--bs-blue);
        transform: translateY(-50%);
        transition: all .35s ease-in-out;
      }
    }

    .current-menu-item > a, 
    a:hover {
      color: var(--bs-blue);
      padding-left: 1.625rem;

      &:before {
        opacity: 1;
      }
    }    
  }
`