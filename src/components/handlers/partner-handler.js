const partnerHandler = {
  name: 'partner',
  priority: 30,
  pattern: 'partner',

  func: async ({ route, params, state, libraries }) => {
    // 1. get products data
    const response = await libraries.source.api.get({
      endpoint: 'partner',
      params: {per_page: 100},
    });

    const entitiesAdded = await libraries.source.populate({ response, state });

    Object.assign(state.source.data[route], {
      isPartnerArchive: true,
      items: entitiesAdded.map(item => ({
        type: item.type,
        title: item.title.rendered,
        id: item.id,
        link: item.link,
        featured_media: item.featured_media
      }))
    });
  }
}

export default partnerHandler;