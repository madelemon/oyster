const newsesHandler = {
  pattern: '/posts/',
  func: async ({ route, params, state, libraries }) => {
    const response = await libraries.source.api.get({
      endpoint: '/posts',
      params: {per_page: 2},
    });

    const items = await libraries.source.populate({ response, state });

    Object.assign(state.source.data[route], {
      isPosType: true,
      items: items.map(item => ({
        type: item.type,
        title: item.title.rendered,
        id: item.id,
        // link: item.link,
        // featured_media: item.featured_media
      }))
    });
  }
}

export default newsesHandler;