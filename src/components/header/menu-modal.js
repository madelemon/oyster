import React, { useEffect } from "react";
import { styled, connect } from "frontity";
import Link from "@frontity/components/link"
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import { gsap, Power1 } from "gsap";
import { TimelineMax } from "gsap/all"; 
import NavOverlay from "./overlayNav";

const MenuModal = ({ state }) => {
  const { menu } = state.theme;
  const { isMobileMenuOpen } = state.theme;
  const isThereLinks = menu != null && menu.length > 0;
  const options = state.source.get("acf-options-page");

    React.useEffect(() => {
      const menuOverflow = document.querySelector('.nav-main > .container-fluid');
      if (isMobileMenuOpen) {
        document.querySelector('header').classList.add('is-menu-open');
        gsap.set('.menu-overlay', { opacity: 1 });
        gsap.to('.menu-overlay', .5, { y: 0, ease: Power1.easeInOut, onComplete: function() {
          gsap.to('.nav-main', .5, { 'opacity': 1, height: '100%', ease: Power1.easeInOut });
          gsap.utils.toArray('.nav-main a').forEach((el, i) => {
            gsap.fromTo(el, 1, { opacity: 0, x: -40, ease: Power1.easeInOut }, { 'opacity': 1, x: 0, delay: i/100, ease: Power1.easeInOut });
          })
        }})
        disableBodyScroll(menuOverflow);
      } else {
        document.querySelector('header').classList.remove('is-menu-open');
        gsap.to('.nav-main', .5, { 'opacity': 0, height: 0, ease: Power1.easeInOut });
        gsap.to('.menu-overlay', .5, { y: '-100%', ease: Power1.easeInOut, onComplete: function() {
          gsap.set('.menu-overlay', { opacity: 0 });
        }})
        clearAllBodyScrollLocks();
      }
      //timeline.reversed(!isMobileMenuOpen)
    }, [isMobileMenuOpen])

  return (
    <>
      <MenuOverlay className="menu-overlay" />
      <MenuContent className="nav-main" as="nav">
        <div className="container-fluid position-relative">
          <div className="row h-100 align-items-center">
            <div className="position-relative">
              <div className="col-md-7 ps-xl-5 offset-md-2">
                <NavOverlay />
              </div>
            </div>
          </div>
          {options.acf.image && (
            <img src={options.acf.image} className="nav-main-img" />
          )}
        </div>
      </MenuContent>
    </>
  );
};

const MenuOverlay = styled.div`
  background-color: var(--bs-dark);
  width: 100vw;
  height: 100vh;
  overflow: hidden auto;
  position: fixed;
  z-index: 2;
  top: 0;
  left: 0;
  opacity: 0;
  transform: translateY(-100%);
  transition: transform 500ms ease-out 0s;
`;

const MenuContent = styled.div`
  z-index: 3;
  color: var(--bs-white);
  position: fixed;
  top: 0;
  left: 0;
  opacity: 0;
  padding-right: 0;
  height: 100vh;
  width: 100%;
  overflow: hidden;

  .container-fluid {
    z-index: 5;
    overflow: auto;
    padding-top: 4.625rem;
    height: 100vh;
    -webkit-overflow-scrolling: touch;

    @media screen and (max-width: 992px) {
      padding-top: 7rem;
      padding-bottom: 0;

      * {
        max-width: 100vw !important;
      }
    }
  }


  .nav-main-img {
    position: absolute;
    bottom: 0;
    z-index: -1;
    right: 0;
    max-height: 81vh; 

    @media screen and (max-width: 768px) {
      width: 100%;
      max-height: 9999px;
      height: auto;
      position: static;
    }
  }

  ul {
    list-style-type: none;
    font-size: 2.375rem;
    padding-left: 0;
    text-transform: uppercase;
    letter-spacing: 1px;
    transition: opacity .3s ease-in-out;

    @media screen and (max-width: 992px) {
      font-size: 1.5rem;
    }

    li {
      transition: opacity .3s ease-in-out;
    }

    &:hover {
      > li {
        opacity: .5;
      }
    }

    > li:hover {
      opacity: 1;
    }

    .current-menu-item > a:hover {
      color: var(--bs-blue) !important;
    }

    a {
      opacity: 0;
      display: inline-block;
      transform: translateX(-40px);
      transition: all 0s ease;
      letter-spacing: .5px;

      &:hover {
        color: var(--bs-white) !important;
      }
    }
  }
`;

const MenuLink = styled(Link)`
  width: 100%;
  display: inline-block;
  outline: 0;
  font-size: 20px;
  text-align: center;
  padding: 1.2rem 0;
  &:hover,
  &:focus {
    background-color: rgba(0, 0, 0, 0.05);
  }
  /* styles for active link */
  &[aria-current="page"] {
    color: yellow;
    font-weight: bold;
    /* border-bottom: 4px solid yellow; */
  }
`;

export default connect(MenuModal);