import React from "react"
import { styled, connect, Global } from "frontity";
import Link from "@frontity/components/link"
import Nav from "./nav";
import Logo from "./logo";
import { CloseIcon, HamburgerIcon } from "./menu-icon";
import MenuModal from "./menu-modal";
import Socials from '../footer/socials'

const Header = ({ state, actions }) => {
  const data = state.source.get(state.router.link)
  const { isMobileMenuOpen } = state.theme;
  let menuClass = ''

  if (isMobileMenuOpen) {
    menuClass = 'is-active';
  }

  return (
    <HeaderEl className={(data.isPage || (data.isPostType && !data.isPartner)) ? 'is-white' : ''}>
      <div className="container-fluid">
        <div className="row align-items-start justify-content-between">
          <Logo />
          <Nav />
          <MenuToggle onClick={actions.oyster.toggleMobileMenu} classList={menuClass}>
          <HamburgerIcon state={isMobileMenuOpen} color="white" size="24px" />
          <Socials section="header" />
          {isMobileMenuOpen ? (
            <>
              {/* Add some style to the body when menu is open,
              to prevent body scroll <Global styles={{ body: { overflowY: "hidden" } }} /> */}
              
            </>
          ) : null }
        </MenuToggle>
        <MenuModal />
        {/* If the menu is open, render the menu modal  {isMobileMenuOpen && <MenuModal />}*/}
       
        </div>
      </div>
    </HeaderEl>
  )
}

export default connect(Header)

const HeaderEl = styled.header`
  position: absolute;
  top: 0;
  padding-top: 0.56rem;
  left: 0;
  right: 0;
  max-width: 100vw;
  z-index: 20;

  .logo {
    z-index: 99;
  }

  &.is-white,
  &.is-menu-open {
    color: var(--bs-white);

    .img-white {
      opacity: 1;
    }

    li a img {
      filter: brightness(0) invert(1);;
    }

    .img-color {
      opacity: 0;
    }

    .current-menu-item a {
      color: var(--bs-blue);
    }

    nav a {
      color: var(--bs-white);

      &:hover {
        color: var(--bs-blue);
      }
    }
  }

  .img-white,
  .img-color {
    transition: all .3s ease-in-out;
  }

  .img-white {
    position: absolute;
    top: 0;
    left: .75rem;
  }

  .img-white {
    opacity: 0;
  }
`

const MenuToggle = styled.button`
  position: absolute;
  right: 1.375rem;
  top: 1rem;
  background: transparent;
  border: 0;
  z-index: 5;
  height: 2.5rem;
  width: 2.5rem;
`;

