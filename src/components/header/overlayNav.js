import React from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link"

/**
 * Two level menu (with one level of child menus)
 */
const NavOverlay = ({ state }) => {
  const current = state.router.link;
  const data = state.source.get(state.router.link)
  const items = state.source.get(`/menu/${state.theme.menuOverlayUrl}/`).items;
  // const page = state.source[data.type][data.id]
  let className = '';
  return data.isReady ? (
    <NavContainer>
      <NavUl>
      {items.map((item) => {
        if (!data.isHome && (item.url.includes(current) || (data.type == 'post' && item.ID === 84) || (data.isPartner && item.title === 'Partners' ))) {
          className = 'current-menu-item';
        } else {
          className = '';
        }
        if (!item.child_items) {
          return (
            <NavItem key={item.ID} className={className} data-id={item.ID}>
              <Link link={item.url}>{item.title}</Link>
            </NavItem>
          );
        } else {
          const childItems = item.child_items;
          return (
              <NavItemWithChild key={item.ID}>
                <Link link={childItems[0].url}>{item.title}</Link>
                <ChildMenu className="col-md-5">
                  {childItems.map((childItem) => {
                    return (
                      <NavItem key={childItem.ID}>
                        <Link link={childItem.url}>{childItem.title}</Link>
                      </NavItem>
                    );
                  })}
                </ChildMenu>
              </NavItemWithChild>
          );
        }
      })}
      </NavUl>
    </NavContainer>
  ) : null;
};

export default connect(NavOverlay);

const NavContainer = styled.nav`
`;

const NavUl = styled.ul`
`;

const NavItem = styled.li`
  &:hover {
    ul {
      z-index: 4;
      opacity: 1 !important;
    }
  }
`;

const NavItemWithChild = styled.li`
  > a {
    min-width: 16rem;
    position: relative;

    @media screen and (max-width: 992px) {
      min-width: 1px;
    }

    &:before,
    &:after {
      content: '';
      width: 1.5rem;
      height: .25rem;
      background: currentColor;
      position: absolute;
      right: 0;
      top: 50%;
      transform: translateY(-50%) rotate(0);
      transition: all .3s ease-in-out;

      @media screen and (max-width: 992px) {
        content: none;
      }
    }

    &:before {
      transform: translateY(-50%) rotate(90deg);
    }
  }

  a {
    pointer-events: none;
  }

  &:hover {

    a {
      pointer-events: all;
    }

    > a:before {
      transform: translateY(-50%) rotate(0);
    }

    ul {
      // z-index: 4;
      pointer-events: all;
      opacity: 1 !important;
    }
  }
`

const ChildMenu = styled.ul`
  position: absolute;
  left: 55%;
  top: 0;
  opacity: 0;
  z-index: 5;
  pointer-events: none;

  @media screen and (max-width: 992px) {
    position: static;
    opacity: 1;
    font-size: 3rem;
    padding: 0 0 1rem 1.5rem !important;
  }
`;
