import React from "react";
import { styled } from "frontity";

export const HamburgerIcon = ({ state, color }) => {
  let cName = ''
  if (state) {
    cName = 'is-active';
  }
  return (
    <Hamburger className={cName}><span /><span /><span /></Hamburger>
  );
};

const Hamburger = styled.div`
  width: 1.5rem;
  height: 1rem;
  transform-origin: center center;
  position: relative;
  transition: transform .2s ease-in-out;

  &.is-active {
    &:hover {
      transform: rotate(.5turn);
    }

    span {
      top: 50% !important;

      &:first-of-type {
        transform: translateY(-50%) rotate(135deg)
      }

      &:nth-of-type(2) {
        transform: scaleX(0);
      }

      &:last-of-type {
        transform: translateY(-50%) rotate(-135deg)
      }
    }
  }

  &:hover {
    span {
      &:first-of-type {
        top: -2px;
      }
      &:last-of-type {
        top: 16px;
      }
    }
  }

  span {
    width: 100%;
    height: 2px;
    position: absolute;
    background: currentColor;
    left: 0;
    transition: all .2s ease-in-out;

    &:first-of-type {
      top: 0;
    }

    &:nth-of-type(2) {
      top: 7px;
    }

    &:nth-of-type(3) {
      top: 14px;
    }
  }
`