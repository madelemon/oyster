import React from "react"
import { connect } from "frontity"
import Link from "@frontity/components/link"
import logoC from "../../assets/img/static/oyster-logo-color.gif";
import logoW from "../../assets/img/static/oyster-logo-white.gif";

const Logo = ({ state }) => {
  const data = state.source.get(state.router.link)

  return (
    <Link link="/" className="logo position-relative col-md-2 w-auto">
      <img src={logoC} width="168" height="78" className="img-color"/>
      <img src={logoW} width="168" height="78" className="img-white"/>
    </Link>
  )
}

export default connect(Logo)