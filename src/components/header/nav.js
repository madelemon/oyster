import React from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link"

const Nav = ({ state }) => {
  const current = state.router.link;
  const data = state.source.get(state.router.link)
  const items = state.source.get(`/menu/${state.theme.menuUrl}/`).items;
  // const page = state.source[data.type][data.id]
  let className = '';
  return data.isReady ? (
    <NavContainer>
      <NavUl>
      {items.map((item) => {
        if (!data.isHome && (item.url.includes(current) || (data.type == 'post' && item.ID === 84) || (data.isPartner && item.title === 'Partners' ))) {
          className = 'current-menu-item';
        } else {
          className = '';
        }
        if (!item.child_items) {
          return (
            <NavItem key={item.ID} className={className} data-id={item.ID}>
              <Link link={item.url}>{item.title}</Link>
            </NavItem>
          );
        } else {
          const childItems = item.child_items;
          return (
            <NavItemWithChild key={item.ID}>
              <NavItem>
                <Link link={item.url}>{item.title}</Link>
              }
              </NavItem>
              <ChildMenu>
                {childItems.map((childItem) => {
                  return (
                    <NavItem key={childItem.ID}>
                      <Link link={childItem.url}>{childItem.title}</Link>
                    </NavItem>
                  );
                })}
              </ChildMenu>
            </NavItemWithChild>
          );
        }
      })}
      </NavUl>
    </NavContainer>
  ) : null;
};

export default connect(Nav);

const NavContainer = styled.nav`
  flex: 0 0 50%;
  width: 50%;
  display: none;
  padding-top: 1rem;
  justify-content: flex-end; 
  padding-right: 6.25rem;
  
  @media screen and (min-width: 1200px) {
    padding-left: 0;
  }

  @media screen and (min-width: 768px) {
    display: flex;
  }

  ul {
    list-style-type: none;
    display: flex;
    align-items: center;
    padding-left: 0;
    margin-bottom: 0;
  }

  li {
    a {
      letter-spacing: .5px;
    }
  }
`;

const NavUl = styled.ul`
`;

const NavItem = styled.li`
  text-transform: uppercase;
  margin: 0 .5rem;

  @media screen and (min-width: 1200px) {
    margin: 0 1rem;
  }

  &.current-menu-item > a {
    color: var(--bs-blue);
  }

  & > a {
    padding: 0 .4rem;
    font-size: 1.0625rem;
    display: inline-block;
    /* Use for semantic approach to style the current link */
    &[aria-current="page"],
    &:hover {
      color: var(--bs-blue);
    }
  }

  &:first-of-type {
    margin-left: 0;

    a {
      padding-left: 0;
    }
  }

  &:last-of-type {
    margin-right: 0;

    a {
      padding-right: 0;
    }
  }
`;