import React, {useEffect} from "react";
import { connect, styled } from "frontity";
import Link from "@frontity/components/link"
import Hero from "./page/hero";
import Content from "./page/content";
import Header from "./header/index";
import Footer from "./footer/index";

const Page = ({ state, actions, libraries }) => {
  const data = state.source.get(state.router.link)
  const page = state.source[data.type][data.id]
  const links = page.sibilings ? page.sibilings : []; 

  return data.isReady ? (
    <>
      <Header />
      <Hero />
      <section className="py-5">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-4">
            {links && (
              <PageUl className="fs-20 lh-1-5 list-unstyled">
                {links.map((link, i) => {
                  return <li key={i} className={(page.id === link.id) ? "current-menu-item" : ''}><Link link={link.url} key={i}>{link.title}</Link></li>
                })}  
              </PageUl>
            )}
            </div>
            <div className="col-md-8 pe-37">
              <Content />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  ) : null 

}

export default connect(Page);

const PageUl = styled.ul`
  list-style-type: none;
  text-transform: uppercase;
  padding-left: 0;
  margin-bottom: 0;

  a {
    position: relative;
    padding-left: 0;

    &:hover {
      color: var(--bs-blue);
    }

    &:before {
      content: '—';
      position: absolute;
      left: 0;
      top: 50%;
      opacity: 0;
      color: var(--bs-blue);
      transform: translateY(-50%);
      transition: all .35s ease-in-out;
    }
  }

  .current-menu-item > a, 
  a:hover {
    color: var(--bs-blue);
    padding-left: 1.625rem;

    &:before {
      opacity: 1;
    }    
  }
`