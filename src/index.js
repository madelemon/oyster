import Root from "./components"
import menuHandler from "./components/handlers/menu-handler";
import partnerHandler from "./components/handlers/partner-handler";
//import newsesHandler from "./components/handlers/newses-handler";
import acfOptionsHandler from "./components/handlers/acf-options-handler";
import allCategoriesHandler from "./components/handlers/categories-handler";

export default {
  name: "oyster-theme",
  roots: {
    oyster: Root
  },
  libraries: {
    source: {
      // Add the custom handler for ACF options defined above.
      handlers: [acfOptionsHandler, menuHandler, allCategoriesHandler]
    },
  },
  actions: {
    oyster: {
      toggleMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = !state.theme.isMobileMenuOpen;
      },
      closeMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = false;
      },
      beforeSSR: async ({ state, actions }) => {
        const data = state.source.get(state.router.link)

        if (state.router.link !== "/partner/") {
          await actions.source.fetch(`/partner/`)
          // await Promise.all([
          //   Object.values(partnerHandler).map(() =>
          //     actions.source.fetch("/partner"),
          //     ),
          //   ]);
        }

       if (state.router.link === "/") {
          await actions.source.fetch(`/latest-news`)
        }

        await actions.source.fetch(`/menu/${state.theme.menuUrl}/`);
        await actions.source.fetch(`/menu/${state.theme.menuOverlayUrl}/`);

        // This will make Frontity wait until the ACF options
        // page has been fetched and it is available
        // using state.source.get("acf-options-page").
        await actions.source.fetch("acf-options-page");

        await actions.source.fetch("all-categories");
      }
    }
  },
  state: {
    oyster: {
      menu: [],
      isMobileMenuOpen: false,
    }
  },
};
