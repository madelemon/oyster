function SvgLine(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 270 469.4"
      width="372"
      height="691"
      {...props}
    >
      <path
        d="M22.1 457.4c135.8 0 245.9-110.1 245.9-245.9C268 130 228.4 57.8 167.4 13"
        clipPath="url(#line_svg__SVGID_2_)"
        fill="none"
        stroke="#058a9c"
        strokeWidth={2}
      />
      <path
        d="M22.1 457.4c135.8 0 245.9-110.1 245.9-245.9C268 130 228.4 57.8 167.4 13"
        clipPath="url(#line_svg__SVGID_2_)"
        fill="none"
        stroke="#0d2851"
        className="path"
        strokeWidth={2}
      />
      <path
        d="M166.9.4c5.9-.8 11.3 3.3 12.1 9.1.8 5.9-3.3 11.3-9.1 12.1-5.9.8-11.3-3.3-12.1-9.1-.9-5.9 3.2-11.3 9.1-12.1"
        clipPath="url(#line_svg__SVGID_2_)"
        fill="#fff"
      />
      <path
        d="M166.9.4c5.9-.8 11.3 3.3 12.1 9.1.8 5.9-3.3 11.3-9.1 12.1-5.9.8-11.3-3.3-12.1-9.1-.9-5.9 3.2-11.3 9.1-12.1M9.5 447.8c5.9-.8 11.3 3.3 12.1 9.1.8 5.9-3.3 11.3-9.1 12.1-5.9.8-11.3-3.3-12.1-9.1-.8-5.9 3.2-11.3 9.1-12.1"
        clipPath="url(#line_svg__SVGID_2_)"
        fill="none"
        stroke="#058a9c"
        strokeWidth={1}
      />
    </svg>
  );
}

export default SvgLine;